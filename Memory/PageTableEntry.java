/****************************************************************
 * 'PageTableEntry.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-29-2017--08:57:12
 *
**/

/****************************************************************
 * Imports and Packages
**/
package osp.Memory;
import osp.Hardware.*;
import osp.Tasks.*;
import osp.Threads.*;
import osp.Devices.*;
import osp.Utilities.*;
import osp.IFLModules.*;

/****************************************************************
 * Class
**/
public class PageTableEntry extends IflPageTableEntry {

  boolean page_faulted = false;
  /****************************************************************
   * Constructor
  **/
  public PageTableEntry( PageTable page_table, int page_number ) {
    super( page_table, page_number );
  }

  /****************************************************************
   * Functions
   * 'do_lock'
   * 'do_unlock'
  **/

  public int do_lock( IORB iorb ) {
    ThreadCB thread = iorb.getThread();
    if ( !isValid() ) {
      if ( getValidatingThread() == null ) {
        PageFaultHandler.handlePageFault( thread, GlobalVariables.MemoryLock, this );
      }//end if
      else if ( getValidatingThread() != thread ) {
        thread.suspend( this );
        if ( thread.getStatus() == GlobalVariables.ThreadKill ) {
          return GlobalVariables.FAILURE;
        }//end if
      }//end else if
    }//end if
    getFrame().incrementLockCount();
    return GlobalVariables.SUCCESS;
  }//end do_lock

  public void do_unlock() {
    getFrame().decrementLockCount();
  }//end do_unlock

}//end class PageTableEntry

/****************************************************************
 * End 'PageTableEntry.java'
**/
