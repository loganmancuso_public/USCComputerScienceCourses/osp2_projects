/****************************************************************
 * 'FrameTableEntry.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-29-2017--08:57:12
 *
**/

/****************************************************************
 * Imports and Packages
**/

package osp.Memory;
import osp.Tasks.*;
import osp.Interrupts.*;
import osp.Utilities.*;
import osp.IFLModules.IflFrameTableEntry;

/****************************************************************
 * Class FrameTableEntry
**/
public class FrameTableEntry extends IflFrameTableEntry {
  /****************************************************************
   * Param Constructor
  **/
  public FrameTableEntry( int  frame_id ) {
    super(  frame_id );
  }//end FrameTableEntry

}//end FrameTableEntry

/****************************************************************
 * End 'FrameTableEntry.java'
**/
