/****************************************************************
 * 'PageFaultHandler.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-29-2017--08:57:12
 *
**/

/****************************************************************
 * Imports and Packages
**/
package osp.Memory;
import java.util.*;
import osp.Hardware.*;
import osp.Threads.*;
import osp.Tasks.*;
import osp.FileSys.FileSys;
import osp.FileSys.OpenFile;
import osp.IFLModules.*;
import osp.Interrupts.*;
import osp.Utilities.*;
import osp.IFLModules.*;

/****************************************************************
 * Class
**/
public class PageFaultHandler extends IflPageFaultHandler {


  /****************************************************************
   * Constructor
  **/


  /****************************************************************
   * Functions
   * 'init'
   * 'do_handlePageFault'
   * 'GetNewFrame'
   * 'SwapIn'
   * 'SwapOut'
  **/
  public static void init() {
  }//end init

  public static int do_handlePageFault( ThreadCB thread, int reference_type, PageTableEntry page ) {
    TaskCB a_task = thread.getTask();
    if ( page.isValid() ) {
      return FAILURE;
    }//end if
    FrameTableEntry new_frame = null;
    new_frame = GetNewFrame();
    //check if memory is available
    if ( new_frame == null ) {
      return NotEnoughMemory;
    }//end if
    Event event = new SystemEvent( "PageFaultHappened" );
    thread.suspend( event );
    page.setValidatingThread( thread );
    new_frame.setReserved( thread.getTask() );
    if ( new_frame.getPage() != null ) {
      PageTableEntry new_page = new_frame.getPage();
      if ( new_frame.isDirty() ) {
        SwapOut( thread, new_frame );
        if ( thread.getStatus() == GlobalVariables.ThreadKill ) {
          page.notifyThreads();
          event.notifyThreads();
          ThreadCB.dispatch();
          return FAILURE;
        }//end if
        new_frame.setDirty( false );
      }//end if
      new_frame.setReferenced( false );
      new_frame.setPage( null );
      new_page.setValid( false );
      new_page.setFrame( null );
    }//end if
    page.setFrame( new_frame );
    SwapIn( thread, page );
    if ( thread.getStatus() == ThreadKill ) {
      if ( new_frame.getPage() != null ) {
        if ( new_frame.getPage().getTask() == thread.getTask() ) {
          new_frame.setPage( null );
        }//end if
      }//end if
      page.notifyThreads();
      page.setValidatingThread( null );
      page.setFrame( null );
      event.notifyThreads();
      ThreadCB.dispatch();
      return FAILURE;
    }//end if
    new_frame.setPage( page );
    page.setValid( true );
    if ( new_frame.getReserved() == a_task ) {
      new_frame.setUnreserved( a_task );
    }//end if
    page.setValidatingThread( null );
    page.notifyThreads();
    event.notifyThreads();
    ThreadCB.dispatch();
    return SUCCESS;
  }//end do_handlePageFault

  private static FrameTableEntry GetNewFrame() {
    FrameTableEntry new_frame = null;
    for ( int i = 0; i < MMU.getFrameTableSize(); i++ ) {
      new_frame = MMU.getFrame( i );
      if ( ( new_frame.getPage() == null ) && ( !new_frame.isReserved() ) && ( new_frame.getLockCount() == 0 ) ) {
        return new_frame;
      }//end if
    }//end for i
    for ( int i = 0; i < MMU.getFrameTableSize(); i++ ) {
      new_frame = MMU.getFrame( i );
      if ( ( !new_frame.isDirty() ) && ( !new_frame.isReserved() ) && ( new_frame.getLockCount() == 0 ) ) {
        return new_frame;
      }//end if
    }//end for
    for ( int i = 0; i < MMU.getFrameTableSize(); i++ ) {
      new_frame = MMU.getFrame( i );
      if ( ( !new_frame.isReserved() ) && ( new_frame.getLockCount() == 0 ) ) {
        return new_frame;
      }//end if
    }//end for
    return MMU.getFrame( MMU.getFrameTableSize() - 1 );
  }//end GetNewFrame

  //Swap in the page
  public static void SwapIn( ThreadCB thread, PageTableEntry page ) {
    TaskCB new_task = page.getTask();
    new_task.getSwapFile().read( page.getID(), page, thread );
  }//end SwapIn

  public static void SwapOut( ThreadCB thread, FrameTableEntry frame ) {
    PageTableEntry new_page = frame.getPage();
    TaskCB new_task = new_page.getTask();
    new_task.getSwapFile().write( new_page.getID(), new_page, thread );
  }//end SwapOut

}//end PageFaultHandler

/****************************************************************
 * End 'PageFaultHandler.java'
**/
