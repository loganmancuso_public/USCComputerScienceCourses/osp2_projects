/****************************************************************
 * 'Vertex.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-29-2017--08:55:02
 *
**/

/****************************************************************
 * Imports and Packages
**/
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/****************************************************************
 * Class 'Vertex'
 * This class represents a vertex in a graph
**/

public class Vertex<Type> {
  /****************************************************************
   * Internal Variables
   * String:
   * name_: name of vertex (some id)
   *
   * Boolean:
   * mark_: if the vert has been visited
   *
   * Int:
   * marked_state_: the state of the node
   *
   * Type:
   * data_: generic type of data to hold in the vert
   *
   * List:
   *   for directed graph shows directionality of edges
   * incoming_edges_: edges coming to vert
   * outgoing_edges_: edges pointing to other vert
  **/
  private List<Edge<Type>> incoming_edges_;
  private List<Edge<Type>> outgoing_edges_;
  private String name_;
  private boolean mark_;
  private int marked_state_;
  private Type data_;

  /****************************************************************
   * Constructor
   * given no parameters
  **/
	public Vertex(){
    set_data( null );
    this.incoming_edges_ = new ArrayList<Edge<Type>>();
    this.outgoing_edges_ = new ArrayList<Edge<Type>>();
  }//end Vertex

  /****************************************************************
   * Param Constructor
   * given name and no data
  **/
	public Vertex( String name ){
    set_name( name );
    set_data( null );    
    this.incoming_edges_ = new ArrayList<Edge<Type>>();
    this.outgoing_edges_ = new ArrayList<Edge<Type>>();
  }//end Vertex

  /****************************************************************
   * Param Constructor
   * given data and no name
  **/
	public Vertex( Type data ){
    set_data( data );
    set_data( null );    
    this.incoming_edges_ = new ArrayList<Edge<Type>>();
    this.outgoing_edges_ = new ArrayList<Edge<Type>>();
  }//end Vertex

  /****************************************************************
   * Param Constructor
   * given name and data
  **/
	public Vertex( String name, Type data ){
    this.mark_ = false;
    set_name( name );
    set_data( data );
    set_data( null );    
    this.incoming_edges_ = new ArrayList<Edge<Type>>();
    this.outgoing_edges_ = new ArrayList<Edge<Type>>();
  }//end Vertex

  /****************************************************************
   * Accessors
   * 'get_name'
   * 'get_data'
   * 'get_marked_state'
   * 'get_mark'
   * 'get_outgoing_edges'
   * 'get_incoming_edges'
   * 'get_incoming_edge'
  *  'get_outgoing_edge'
  **/

  public String get_name(){
    return this.name_ != null ? this.name_ : "Name Not Set";
  }//end get_name

  public Type get_data(){
    return this.data_ != null ? this.data_ : null;
  }//end get_data

  public int get_marked_state() {
    return this.marked_state_;
  }//end get_marked_state

  public boolean get_mark() {
    return this.mark_;
  }//end get_mark

  public List<Edge<Type>> get_outgoing_edges() {
    return this.outgoing_edges_;
  }//end get_outgoing_edges

  public List<Edge<Type>> get_incoming_edges() {
    return this.incoming_edges_;
  }//end get_incoming_edges

  public Edge<Type> get_incoming_edge( int here ) {
    return this.incoming_edges_.get( here );
  }//end get_incoming_edge

  public Edge<Type> get_outgoing_edge( int here ) {
    return this.outgoing_edges_.get( here );
  }//end get_outgoing_edge

  /****************************************************************
   * Mutators
   * 'set_data'
   * 'set_marked_state'
   * 'set_name'
   **/

  public void set_data( Type data ) {
    this.data_ = data;
  }//end set_data

  public void set_marked_state( int state ) {
    this.marked_state_ = state;
  }//end set_marked_state

  public void set_name( String name ) {
    this.name_ = name;
  }//end set_name

  /****************************************************************
   * Functions
   * 'get_incoming_edge_count'
   * 'get_outgoing_edge_count'
   * 'add_edge'
   * 'add_outgoing_edge'
   * 'add_outgoing_edge'
   * 'has_edge'
   * 'remove'
   * 'find_edge' (vertex)
   * 'find_edge' (edge)
   * 'cost'
   * 'has_edge'
   * 'visited'
   * 'has_edge'
   * 'visit'
  * 'clear_mark'
   * 'to_string'
  **/

  public int get_incoming_edge_count() {
    return this.incoming_edges_.size();
  }//end get_incoming_edge_count

  public int get_outgoing_edge_count() {
    return this.outgoing_edges_.size();
  }//end get_outgoing_edge_count

  public boolean add_edge( Edge<Type> edge ) {
    if( edge.get_previous() == this ) {
      this.outgoing_edges_.add(edge);
    }//end if
    else if( edge.get_next() == this ) {
      this.incoming_edges_.add( edge );
    }//end else if
    else {
      return false;
    }//end else
    return true;
  }//end add_edge

  public void add_outgoing_edge( Vertex<Type> to, int cost ) {
    Edge<Type> out = new Edge<Type>( this, to, cost );
    this.outgoing_edges_.add( out );
  }//end add_outgoing_edge

  public void add_incoming_edge( Vertex<Type> from, int cost ) {
    Edge <Type> out = new Edge<Type>( this, from, cost );
    this.incoming_edges_.add( out );
  }//end add_outgoing_edge

  public boolean has_edge( Edge<Type> edge ) {
    if( edge.get_previous() == this ) {
      return this.incoming_edges_.contains( edge );
    }//end if
    else if (edge.get_next() == this ) {
      return this.outgoing_edges_.contains( edge );
    }//end else if
    else {
      return false;
    }//end else
  }//end has_edge

  public boolean remove( Edge<Type> edge ) {
    if( edge.get_previous() == this ) {
      this.incoming_edges_.remove( edge );
    }//end if
    else if( edge.get_next() == this ) {
      this.outgoing_edges_.remove( edge );
    }//end else if
    else {
      return false;
    }//end else
    return true;
  }//end remove

  public Edge<Type> find_edge( Vertex<Type> destination ) {
    for( Edge<Type> edge : this.outgoing_edges_ ) {
      if( edge.get_next() == destination ) {
        return edge;
      }//end if
    }//end for
    return null;
  }//end find_edge

  public Edge<Type> find_edge( Edge<Type> destination ) {
    if( this.outgoing_edges_.contains( destination ) ) {
      return destination;
    }//end for
    else {
      return null;
    }//end else
  }//end find_edge

  public int cost( Vertex<Type> destination ) {
    if( destination == this ) {
      return 0;
    }//end if
    Edge<Type> edge = find_edge( destination );
    int cost = Integer.MAX_VALUE; //infinite weight
    if( edge != null ) {
      cost = edge.get_weight();
    }//end if
    return cost;
  }//end cost

  public boolean has_edge( Vertex<Type> destination ) {
    return( find_edge( destination ) != null );
  }//end has_edge

  public boolean visited() {
    return this.mark_;
  }//end visited

  public void mark() {
    this.mark_ = true;
  }//end has_edge

  public void visit() {
    mark();
  }//end visit

  public void clear_mark() {
    this.mark_ = false;
  }//end clear_mark

  public String to_string() {
    StringBuffer out_string = new StringBuffer();
    out_string.append("\tName:\t" + get_name() + "\n");
    out_string.append("\tMarked State\t" + get_marked_state() + "\n");
    out_string.append("\tIncoming Edges {\n");
    for( Edge<Type> edge : this.incoming_edges_ ) {
      out_string.append(edge.to_string() + "\n");
    }//end for
   out_string.append("}\n");
    out_string.append("\tOutgoing Edges {\n");
    for( Edge<Type> edge : outgoing_edges_ ) {
      out_string.append(edge.to_string() + "\n");
    }//end for
    out_string.append("}\n");
    //out_string.append("\tData:\t" + this.data_.toString() + "\n");
    return out_string.toString();
  }//end to_string

}//end Class Vertex

/****************************************************************
 * End 'Vertex.java'
**/
