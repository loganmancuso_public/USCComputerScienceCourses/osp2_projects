/****************************************************************
 * 'Edge.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-29-2017--08:55:02
 *
**/

/****************************************************************
 * Imports and Packages
**/
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/****************************************************************
 * Class 'Edge'
**/
public class Edge<Type> {

  /****************************************************************
   * Internal Variables
   * Vertex:
   * previous_: prev vertex
   * next_: next vertex
   *
   * Boolean:
   * mark_: marked state of edge
   *
   * Int:
   * weight_: cost to traverse edge
  **/
  private Vertex<Type> previous_;
  private Vertex<Type> next_;
  private int weight_;
  private boolean mark_;

  /****************************************************************
   * Param Constructor
   * given vertex prev and next but no weight
  **/

	public Edge( Vertex<Type> previous, Vertex<Type> next ){
    this.previous_ = previous;
    this.next_ = next;
    set_weight ( 0 );
  }//end Edge

  /****************************************************************
   * Param Constructor
   * given vertex prev and next and weight
  **/

	public Edge( Vertex<Type> previous, Vertex<Type> next, int weight ){
    this.previous_ = previous;
    this.next_ = next;
    this.mark_ = false;
    set_weight ( weight );
  }//end Edge


  /****************************************************************
   * Accessors
   * 'get_next'
   * 'get_previous'
   * 'get_weight'
   * 'get_marked'
  **/

  public Vertex<Type> get_next() {
    return this.next_;
  }//end get_next

  public Vertex<Type> get_previous() {
    return this.previous_;
  }//end get_previous

  public int get_weight() {
    return this.weight_;
  }//end get_weight

  public boolean get_marked() {
    return this.mark_;
  }//end get_marked

  /****************************************************************
   * Mutators
   * 'set_weight'
   **/

  public void set_weight( int weight ) {
    this.weight_ = weight;
  }

   /****************************************************************
   * Functions
   * 'mark'
   * 'clear_mark'
   * 'to_string'
  **/
  public void mark() {
    this.mark_ = true;
  }
  public void clear_mark() {
    this.mark_ = false;
  }

  /****************************************************************
   * 'to_string'
  **/
  public String to_string() {
    StringBuffer out_string = new StringBuffer();
    out_string.append("\t[Previous Vertex: " + this.previous_.get_name() + "|" );
    out_string.append("\tNext Vertex: " + this.next_.get_name() + "|");
    out_string.append("\tWeight: " + get_weight() + "|");
    out_string.append("\tMarked State: " + get_marked() + "  ]");
    return out_string.toString();
  }//end to_string

}//end class Edge

/****************************************************************
 * End 'Edge.java'
**/
