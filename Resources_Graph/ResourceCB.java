/****************************************************************
 * 'ResourceCB.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-01-2017--07:18:21
 *
**/

/****************************************************************
 * Imports and Packages
**/
package osp.Resources;
import java.util.*;
import osp.IFLModules.*;
import osp.Tasks.*;
import osp.Threads.*;
import osp.Utilities.*;
import osp.Memory.*;

/****************************************************************
 * Class 'ResourceCB'
**/

public class ResourceCB extends IflResourceCB {
  /****************************************************************
   * Global Variables
  **/
  private static Graph<ThreadCB> wait_for_graph_;
  private static int size_;

  /****************************************************************
   * Constructor
  **/
  public ResourceCB(int qty) {
    super( qty );
  }//end ResourceCB

  /****************************************************************
   * Function 'init'
  **/
  public static void init() {
    size_ = ResourceTable.getSize();
    wait_for_graph_ = new Graph<ThreadCB>();
  }//end init

  /****************************************************************
   *
  **/
  public RRB  do_acquire(int quantity) {
    ThreadCB a_thread = null;
    try {
      a_thread = MMU.getPTBR().getTask().getCurrentThread();
    }//end try
    catch( NullPointerException exception ) {
      System.out.println( exception.getMessage() );
    }//end catch
    if( ( this.getAvailable() < quantity )
        || ( quantity < 0 )
        || ( quantity + this.getAllocated( a_thread )
            > this.getMaxClaim( a_thread ) ) )
    {
      return null;
    } //end if
    wait_for_graph_.add_vertex( a_thread );
    RRB a_request = new RRB( a_thread, this, quantity );
    switch( IflResourceCB.getDeadlockMethod() ) {
    case GlobalVariables.Avoidance:
      if( ResourceCB.bankers_algorithm() ) {
        a_request.setStatus( GlobalVariables.Granted );
        a_request.grant();
      } //end if
      else {
        a_request.setStatus( GlobalVariables.Suspended );
        wait_for_graph_.add_vertex( a_request );
        //ResourceCB.rrb_array_list_.add(a_request);
        a_thread.suspend(a_request);
      } //end else
      break;
    case GlobalVariables.Detection:
      if (quantity <= this.getAvailable()) {
        a_request.setStatus(GlobalVariables.Granted);
        a_request.grant();
      } //end if
      else if (this.getTotal() < quantity) {
        return null;
      } //end else if
      else {
        a_request.setStatus(GlobalVariables.Suspended);
        ResourceCB.rrb_array_list_.add(a_request);
        a_thread.suspend(a_request);
      } //end else
      break;
    default:
      System.out.println("Error in Swtich Case");
      break;
    }//end switch
    return a_request;


  }//end do_acquire

  /****************************************************************
   *
  **/
  public static Vector do_deadlockDetection() {

  }//end do_deadlockDetection

  /****************************************************************
   *
  **/
  public static void do_giveupResources(ThreadCB thread) {
    // your code goes here
  }//end do_giveupResources

  /****************************************************************
   *
  **/
  public void do_release(int quantity) {

  }//end do_release

  /****************************************************************
   *
  **/
  public static void atError() {
    System.out.println("Error");
  }//end atError

  /****************************************************************
   *
  **/
  public static void atWarning() {
    System.out.println("Warning");
  }//end atWarning

}//end ResourceCB

/****************************************************************
 * End 'ResourceCB.java'
**/
