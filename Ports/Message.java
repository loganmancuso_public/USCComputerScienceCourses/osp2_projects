/****************************************************************
 * 'Message.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-29-2017--08:48:49
 *
**/

/****************************************************************
 * Imports and Packages
**/
package osp.Ports;
import osp.Utilities.*;
import osp.IFLModules.*;

/****************************************************************
 * Class 'Message'
**/
public class Message extends IflMessage {

  /****************************************************************
   * Param Constructor 
  **/
  public Message( int length ) {
    super( length ); //call parent class
  }//end Message

}//end class Message

/****************************************************************
 * End 'Message.java'
**/
